from django.shortcuts import render
from receipts.models import Receipt

# Create your views here.


def receipt_list(request):
    receipts = Receipt.objects.all()
    # print("vendor==" + receipts[0].vendor)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/receipt_list.html", context)
